﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FractionSwitch : MonoBehaviour
{
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;
    Image fractionSprite;
    public Sprite modelSprite;
    public Sprite fractionSprite2;
    //public bool fractionMode = false;
    private int fractionCount = 0;
    void Start()
    {
        
        //Fetch the Raycaster from the GameObject (the Canvas)
        m_Raycaster = GetComponent<GraphicRaycaster>();
        //Fetch the Event System from the Scene
        m_EventSystem = GetComponent<EventSystem>();
        fractionSprite = GetComponent<Image>();
    }

    void Update()
    {
        //Check if the left Mouse button is clicked
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            //Set up the new Pointer Event
            m_PointerEventData = new PointerEventData(m_EventSystem);
            //Set the Pointer Event Position to that of the mouse position
            m_PointerEventData.position = Input.mousePosition;

            //Create a list of Raycast Results
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            m_Raycaster.Raycast(m_PointerEventData, results);

            //fractionCount goes up by 1
            fractionCount++;

            //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
            foreach (RaycastResult result in results)

            {
                
                if (fractionCount % 2 == 0)
                {
                 fractionSprite.sprite = fractionSprite2;
                 Debug.Log("Even Number of Clicks" + fractionCount);
                } else
                {
                  fractionSprite.sprite = modelSprite;
                }
                   
                //fractionSprite.sprite = modelSprite;
                Debug.Log("Hit " + result.gameObject.name);
            }
        }
    }
}