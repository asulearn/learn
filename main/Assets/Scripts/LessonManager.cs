﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vuforia;

public class LessonManager : MonoBehaviour {

    public List<GameObject> cards;
    public Camera AR;
    public AudioClip click, good, bad;

    private AudioSource audio;

    private UnityEngine.UI.Image panel;
    private Button hintB, prevButton, CheckButton;
    private bool buttonState = false;
    

    private string scene;
    private Color c;

    private GameObject[] hintPanels, correctPanels;
    private Text hintText, noteText, problemText;

    private Fraction answer;

    private bool soundPlayed;

    private string zone;
    private string zone1;
    private string zone2;

    private Fraction f1;
    private Fraction f2;

    void Start () {
        scene = SceneManager.GetActiveScene().name;

        panel = GameObject.Find("Answer Panel").GetComponent<UnityEngine.UI.Image>();
        hintB = GameObject.Find("HintButton").GetComponent<Button>();
        //nextButton = GameObject.Find("NextButton").GetComponent<Button>();
        prevButton = GameObject.Find("PrevButton").GetComponent<Button>();
        CheckButton = GameObject.Find("CheckButton").GetComponent<Button>();
        hintText = GameObject.Find("HintText").GetComponent<Text>();
        noteText = GameObject.Find("NoteText").GetComponent<Text>();
        problemText = GameObject.Find("ProblemText").GetComponent<Text>();

        audio = GetComponent<AudioSource>();

        soundPlayed = false;
        
        
        hintB.interactable = false;
        CheckButton.interactable = true;

        c = panel.color;
        c.a = 0;
        panel.color = c;

        hintPanels = GameObject.FindGameObjectsWithTag("HintPanel");
        correctPanels = GameObject.FindGameObjectsWithTag("CorrectPanel");

        HideHint();
        HideCorrect();
    }
	
	void Update () {
        Debug.Log("the button state upon initailzation is:" +buttonState);

        if (cards.Count == 0)
        {
            clearPanel();
            return;
        }

        switch(scene)
        {
           case "Add1":
                answer = new Fraction(1, 2);
                zone = "right";
                hintText.text = "No Hint!";
                noteText.text = "2/4 is equal to 1/2";
                problemText.text = "Problem 1 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }
                else if (buttonState == true && cardIn(zone) != null)
                {
                    checkAnswer(createFraction(cardIn(zone)), answer, zone);
                }
               

                

                break;

           case "Add2":
                answer = new Fraction(2, 3);
                zone = "right";
                hintText.text = "Can you reduce any of the fractions?";
                noteText.text = "1 is equal to 1/1";
                problemText.text = "Problem 2 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Add3":
                answer = new Fraction(3, 5);
                zone = "middle";
                hintText.text = "Find the least common denominator";
                noteText.text = "When the base matches the numerator the fraction is equal to 1";
                problemText.text = "Problem 3 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Add4":
                zone1 = "leftDouble";
                zone2 = "middleDouble";
                f1 = f2 = new Fraction(0, 0);
                answer = new Fraction(3, 4);
                hintText.text = "Find the least common denominator";
                noteText.text = "It is easier when everything has the same bases";
                problemText.text = "Problem 4 of 5";

                if (cardIn(zone1) != null)
                {
                    f1 = createFraction(cardIn(zone1));
                }
                if (cardIn(zone2) != null)
                {
                    f2 = createFraction(cardIn(zone2));
                }

                if (!f1.ToString().Equals("0/0") && !f2.ToString().Equals("0/0"))
                {
                    checkAnswer(f1 + f2, answer);
                }
               
                break;

            case "Add5":
                zone1 = "leftDouble";
                zone2 = "middleDouble";
                f1 = f2 = new Fraction(0, 0);
                answer = new Fraction(7, 12);
                hintText.text = "Find the least common denominator";
                noteText.text = "You did it!";
                problemText.text = "Problem 5 of 5";

                if (cardIn(zone1) != null)
                {
                    f1 = createFraction(cardIn(zone1));
                }
                if (cardIn(zone2) != null)
                {
                    f2 = createFraction(cardIn(zone2));
                }

                if (!f1.ToString().Equals("0/0") && !f2.ToString().Equals("0/0"))
                {
                    checkAnswer(f1 + f2, answer);
                }

                break;

            case "Sub1":
                answer = new Fraction(2, 5);
                zone = "right";
                hintText.text = "Subtract the numerators (the top numbers)";
                noteText.text = "When the bases are the same you can just bring the base across and just focus on the top.";
                problemText.text = "Problem 1 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Sub2":
                answer = new Fraction(1, 2);
                zone = "right";
                hintText.text = "Try changing 1 to 2/2";
                noteText.text = "Whole numbers are always over 1";
                problemText.text = "Problem 2 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Sub3":
                answer = new Fraction(1, 6);
                zone = "right";
                hintText.text = "What's the least common denominator";
                noteText.text = "Finding the least common denominator lets you subtract the numerators and leave the base alone.";
                problemText.text = "Problem 3 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Sub4":
                answer = new Fraction(1, 1);
                zone = "right";
                hintText.text = "If the numerator is equal to the denominator the fraction is equal to 1";
                noteText.text = "Add the two parts of the equation to get the missing part";
                problemText.text = "Problem 4 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Sub5":
                answer = new Fraction(3, 5);
                zone = "left";
                hintText.text = "Add the numerators";
                noteText.text = "Are you ready for something harder?";
                problemText.text = "Problem 5 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Mul1":
                answer = new Fraction(1, 4);
                zone = "right";
                hintText.text = " Multiply the numerators together and multiply the denominators together";
                noteText.text = "Remember to simplify the fraction";
                problemText.text = "Problem 1 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Mul2":
                answer = new Fraction(1, 5);
                zone = "right";
                hintText.text = "Remember to simplify the fraction";
                noteText.text = "Reduce Reduce Reduce";
                problemText.text = "Problem 2 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Mul3":
                answer = new Fraction(3, 4);
                zone = "right";
                hintText.text = "Remember to multiply the numerators together and the denominators together";
                noteText.text = "You’re doing great";
                problemText.text = "Problem 3 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Mul4":
                answer = new Fraction(1, 2);
                zone = "left";
                hintText.text = "What has a common denominator of 6?";
                noteText.text = "Remember to use the product to find the missing part of the equation";
                problemText.text = "Problem 4 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Mul5":
                zone1 = "leftDouble";
                zone2 = "middleDouble";
                f1 = f2 = new Fraction(0, 0);
                answer = new Fraction(1, 3);
                hintText.text = "What are some equivalent fractions to ⅓?";
                noteText.text = "You’re a math wiz!";
                problemText.text = "Problem 5 of 5";

                if (cardIn(zone1) != null)
                {
                    f1 = createFraction(cardIn(zone1));
                }
                if (cardIn(zone2) != null)
                {
                    f2 = createFraction(cardIn(zone2));
                }

                if (!f1.ToString().Equals("0/0") && !f2.ToString().Equals("0/0"))
                {
                    checkAnswer(f1 * f2, answer);
                }

                break;

            case "Div1":
                answer = new Fraction(2, 3);
                zone = "right";
                hintText.text = "What is a number times 1?";
                noteText.text = "Anything divided by 1 is the original number";
                problemText.text = "Problem 1 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Div2":
                answer = new Fraction(1, 1);
                zone = "right";
                hintText.text = "Remember that 5/5 is equal to 1";
                noteText.text = "Remember to flip the second fraction";
                problemText.text = "Problem 2 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Div3":
                answer = new Fraction(1, 3);
                zone = "middle";
                hintText.text = "Remember to change the whole number to a fraction with the same base as the first part of the equation.";
                noteText.text = "Remember to flip the second fraction and then multiply across";
                problemText.text = "Problem 3 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Div4":
                answer = new Fraction(2, 3);
                zone = "right";
                hintText.text = "Flip the second fraction and then multiply them.";
                noteText.text = "You’re doing great! Keep going";
                problemText.text = "Problem 4 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;

            case "Div5":
                answer = new Fraction(3, 4);
                zone = "middle";
                hintText.text = "Try to divide the first part of the equation by the quotient.";
                noteText.text = "Congratulations!";
                problemText.text = "Problem 5 of 5";

                if (cardIn(zone) == null)
                {
                    clearPanel();
                    return;
                }

                checkAnswer(createFraction(cardIn(zone)), answer, zone);

                break;
        }
       
    }

    bool inLeft(GameObject c)
    {
        if (AR.WorldToScreenPoint(c.transform.position).y > AR.pixelHeight / 2) { return true; }
        else { return false; }
    }

    bool inMiddle(GameObject c)
    {
        if (AR.WorldToScreenPoint(c.transform.position).y > AR.pixelHeight / 4 && AR.WorldToScreenPoint(c.transform.position).y < (AR.pixelHeight / 4) * 3) { return true; }
        else { return false; }
    }

    bool inRight(GameObject c)
    {
        if (AR.WorldToScreenPoint(c.transform.position).y < AR.pixelHeight / 2) { return true; }
        else { return false; }
    }
    
    bool inLeftDouble(GameObject c)
    {
        if (AR.WorldToScreenPoint(c.transform.position).y > (AR.pixelHeight / 2) + (AR.pixelHeight / 8)) { return true; }
        else { return false; }
    }

    bool inMiddleDouble(GameObject c)
    {
        if (AR.WorldToScreenPoint(c.transform.position).y > AR.pixelHeight / 4 && AR.WorldToScreenPoint(c.transform.position).y < (AR.pixelHeight / 2) + (AR.pixelHeight / 8)) { return true; }
        else { return false; }
    }

    GameObject cardIn(string pLoc)
    {
        switch (pLoc)
        {
            case "left":
                for (int i = 0; i < cards.Count; i++)
                {
                    if (inLeft(cards[i]))
                    {
                        return cards[i];
                    }
                }
                break;
            case "middle":
                for (int i = 0; i < cards.Count; i++)
                {
                    if (inMiddle(cards[i]))
                    {
                        return cards[i];
                    }
                }
                break;
            case "right":
                for (int i = 0; i < cards.Count; i++)
                {
                    if (inRight(cards[i]))
                    {
                        return cards[i];
                    }
                }
                break;
            case "leftDouble":
                for (int i = 0; i < cards.Count; i++)
                {
                    if (inLeftDouble(cards[i]))
                    {
                        return cards[i];
                    }
                }
                break;
            case "middleDouble":
                for (int i = 0; i < cards.Count; i++)
                {
                    if (inMiddleDouble(cards[i]))
                    {
                        return cards[i];
                    }
                }
                break;
        }

        return null;
    }

    void answerCorrect(bool correct)
    {
        if(correct)
        {
            if (!soundPlayed)
            {
                audio.PlayOneShot(good);
                soundPlayed = true;
            }
            
            c = Color.green;
            c.a = 0.5f;
            panel.color = c;
            //nextButton.interactable = true;
            hintB.interactable = false;
            ShowCorrect();
        } else {
            if (!soundPlayed)
            {
                audio.PlayOneShot(bad);
                soundPlayed = true;
            }

            c = Color.red;
            c.a = 0.5f;
            panel.color = c;
            //nextButton.interactable = false;
            hintB.interactable = true;
        }
    }

    void clearPanel()
    {
        c.a = 0f;
        panel.color = c;
        soundPlayed = false;
    }

    void cardsInPanel(string pLoc)
    {
        bool anyInPanel = false;

        switch (pLoc)
        {
            case "left":
                cards.ForEach(card =>
                {
                    if (inLeft(card))
                    {
                        anyInPanel = true;
                    }
                });
                break;

            case "middle":
                cards.ForEach(card =>
                {
                    if (inMiddle(card))
                    {
                        anyInPanel = true;
                    }
                });
                break;

            case "right":
                cards.ForEach(card =>
                {
                    if (inRight(card))
                    {
                        anyInPanel = true;
                    }
                });
                break;
            case "leftDouble":
                cards.ForEach(card =>
                {
                    if (inLeftDouble(card))
                    {
                        anyInPanel = true;
                    }
                });
                break;
            case "middleDouble":
                cards.ForEach(card =>
                {
                    if (inMiddleDouble(card))
                    {
                        anyInPanel = true;
                    }
                });
                break;

            default:
                Debug.Log("switch for checking any card isn't working");
                break;
        }

        if (!anyInPanel)
        {
            clearPanel();
        }
    }

    Fraction createFraction(GameObject card)
    {
        Fraction r = new Fraction(0, 0);

        r = new Fraction(Int32.Parse(card.name.Split('_')[0]), Int32.Parse(card.name.Split('_')[1]));

        return r;
    }

    void checkAnswer(Fraction f1, Fraction f2)
    {
        if (f1.Simplify().Equals(f2.Simplify()))
        {
            answerCorrect(true);
        } else
        {
            answerCorrect(false);
        }
    }

    void checkAnswer(Fraction f1, Fraction f2, string z)
    {
        cardsInPanel(z);

        if (f1.Simplify().Equals(f2.Simplify()))
        {
            answerCorrect(true);
            
        }
        else
        {
            answerCorrect(false);
            buttonState = false;
        }

        cardsInPanel(z);
    }

    public void ShowHint()
    {
        audio.PlayOneShot(click);
        foreach (GameObject g in hintPanels)
        {
            g.SetActive(true);
        }
    }

    public void HideHint()
    {
        audio.PlayOneShot(click);
        foreach (GameObject g in hintPanels)
        {
            g.SetActive(false);
        }
    }

    public void ShowCorrect()
    {
        foreach (GameObject g in correctPanels)
        {
            g.SetActive(true);
        }
    }

    public void HideCorrect()
    {
        audio.PlayOneShot(click);
        foreach (GameObject g in correctPanels)
        {
            g.SetActive(false);
        }
    }

    public void ButtonClicked()
    {
        
        buttonState = true;
        Debug.Log(buttonState);
        //Output this to console when the Button3 is clicked
        //Debug.Log("Button clicked = ");
    }
}
