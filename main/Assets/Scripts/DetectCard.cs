﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class DetectCard : MonoBehaviour, Vuforia.ITrackableEventHandler {

    private TrackableBehaviour mTrackableBehaviour;

    private LessonManager lessonManager;
    public GameObject model3d = null;
    private GameObject modelFraction = null;

	void Start () {
        lessonManager = GameObject.Find("Lesson Manager").GetComponent<LessonManager>();

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
	}

    public void OnTrackableStateChagned (TrackableBehaviour.Status newStatus)
    {
        OnTrackableStateChanged(default(TrackableBehaviour.Status), newStatus);
    }

    public void OnTrackableStateChanged(
         TrackableBehaviour.Status previousStatus,
         TrackableBehaviour.Status newStatus)
    {
        if (newStatus == null)
        {
            throw new System.ArgumentNullException("newStatus");
        }

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            lessonManager.cards.Add(this.gameObject);

            if (model3d == null)
            {
                return;
            }

            modelFraction = Instantiate(model3d);
            modelFraction.transform.parent = transform;
            modelFraction.transform.localPosition = new Vector3(-0.05f, .35f, 0);
            modelFraction.transform.localRotation = Quaternion.Euler(90, 0, 180);
        }
        else
        {
            lessonManager.cards.Remove(this.gameObject);
            foreach (Transform child in this.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }

}
