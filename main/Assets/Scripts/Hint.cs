﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hint : MonoBehaviour {

    private GameObject[] hintPanels;
    static Text hintText;

	void Start () {
        hintPanels = GameObject.FindGameObjectsWithTag("HintPanel");

        HideHint();
    }
	
	public void ShowHint()
    {
        foreach (GameObject g in hintPanels)
        {
            g.SetActive(true);
        }
    }

    public void HideHint()
    {
        foreach (GameObject g in hintPanels)
        {
            g.SetActive(false);
        }
    }

    internal static void changeHint(string v)
    {
        hintText.text = v;
    }
}
