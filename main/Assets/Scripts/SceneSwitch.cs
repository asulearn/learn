﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour {

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Lessons()
    {
        SceneManager.LoadScene("Lessons");
    }

    public void Settings()
    {
        SceneManager.LoadScene("Settings");
    }

    public void About()
    {
        SceneManager.LoadScene("About");
    }

    public void Add()
    {
        SceneManager.LoadScene("Addition");
    }

    public void Add1()
    {
        SceneManager.LoadScene("Add1");
    }

    public void Add2()
    {
        SceneManager.LoadScene("Add2");
    }

    public void Add3()
    {
        SceneManager.LoadScene("Add3");
    }

    public void Add4()
    {
        SceneManager.LoadScene("Add4");
    }

    public void Add5()
    {
        SceneManager.LoadScene("Add5");
    }

    public void Subtract()
    {
        SceneManager.LoadScene("Subtraction");
    }

    public void Sub1()
    {
        SceneManager.LoadScene("Sub1");
    }

    public void Sub2()
    {
        SceneManager.LoadScene("Sub2");
    }

    public void Sub3()
    {
        SceneManager.LoadScene("Sub3");
    }

    public void Sub4()
    {
        SceneManager.LoadScene("Sub4");
    }

    public void Sub5()
    {
        SceneManager.LoadScene("Sub5");
    }

    public void Multi()
    {
        SceneManager.LoadScene("Multiplication");
    }

    public void Mul1()
    {
        SceneManager.LoadScene("Mul1");
    }

    public void Mul2()
    {
        SceneManager.LoadScene("Mul2");
    }

    public void Mul3()
    {
        SceneManager.LoadScene("Mul3");
    }

    public void Mul4()
    {
        SceneManager.LoadScene("Mul4");
    }

    public void Mul5()
    {
        SceneManager.LoadScene("Mul5");
    }

    public void Divide()
    {
        SceneManager.LoadScene("Division");
    }

    public void Div1()
    {
        SceneManager.LoadScene("Div1");
    }

    public void Div2()
    {
        SceneManager.LoadScene("Div2");
    }

    public void Div3()
    {
        SceneManager.LoadScene("Div3");
    }

    public void Div4()
    {
        SceneManager.LoadScene("Div4");
    }

    public void Div5()
    {
        SceneManager.LoadScene("Div5");
    }

    public void ImageTargets()
    {
        Application.OpenURL("https://drive.google.com/file/d/1FdXI_nm9n4k0qGh3rHpLhoCf7--1VR06/view?usp=sharing");
    }

}
